package com.welbits.flatland.processor

import freemarker.cache.ClassTemplateLoader
import freemarker.template.Configuration
import java.io.OutputStreamWriter
import java.io.Writer
import javax.annotation.processing.AbstractProcessor
import javax.annotation.processing.RoundEnvironment
import javax.lang.model.SourceVersion
import javax.lang.model.element.TypeElement

internal class FlatlandProcessorImpl() : AbstractProcessor() {

    override fun process(annotations: Set<TypeElement>, roundEnv: RoundEnvironment): Boolean {
        try {
            val elements = roundEnv.getElementsAnnotatedWith(FlatModel::class.java)
            Log.mesagger = processingEnv.messager

            val cfg = Configuration(Configuration.VERSION_2_3_22)
            cfg.defaultEncoding = "UTF-8";
            cfg.templateLoader = ClassTemplateLoader(FlatlandProcessorImpl::class.java, "/templates")
            elements.forEach {
                val template = cfg.getTemplate("java_template.ftl")
                val flat = createFlatClass(it, processingEnv)
                template.process(flat, DebugWriter())
            }
            return true
        } catch (e: Exception) {
            Log.exception(e);
            throw e
        }
    }

    override fun getSupportedAnnotationTypes() = kotlin.setOf<String>(FlatModel::class.java.canonicalName)

    override fun getSupportedSourceVersion() = SourceVersion.latestSupported()
}