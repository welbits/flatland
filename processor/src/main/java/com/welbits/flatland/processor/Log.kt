package com.welbits.flatland.processor

import java.io.Writer
import javax.annotation.processing.Messager
import javax.tools.Diagnostic
import kotlin.properties.Delegates

public object Log {
    public var mesagger  by Delegates.notNull<Messager>()

    fun trace(message: String) {
        mesagger.printMessage(Diagnostic.Kind.NOTE, message)
    }

    fun error(message: String) {
        mesagger.printMessage(Diagnostic.Kind.ERROR, message)
        throw IllegalArgumentException(message)
    }

    fun exception(e: Throwable) {
        error(e.getMessage() ?: "No message")
        e.getStackTrace().forEach { Log.error(it.toString()) }
    }
}

public class DebugWriter : Writer() {

    private val sb = StringBuilder("\n")

    override fun write(buffer: CharArray?, off: Int, len: Int) {
        buffer ?: return
        sb.append(String(buffer, off, len))
    }

    override fun flush() {
        Log.trace(sb.append("\n").toString())
    }

    override fun close() {
    }
}