package com.welbits.flatland.processor

import java.util.*
import javax.annotation.processing.ProcessingEnvironment
import javax.lang.model.element.Element
import javax.lang.model.element.Parameterizable
import javax.lang.model.type.DeclaredType
import javax.lang.model.type.PrimitiveType
import javax.lang.model.type.TypeKind
import javax.lang.model.util.ElementFilter
import javax.lang.model.util.SimpleTypeVisitor6
import javax.lang.model.util.Types


public data class FlatClass(val simpleName: String,
                            val type: String,
                            val packageName: String,
                            val primitiveFields: MutableList<FlatBasicField> = ArrayList(),
                            val objectFields: MutableList<FlatBasicField> = ArrayList(),
                            val primitiveListFields: MutableList<FlatListField> = ArrayList(),
                            val objectListFields: MutableList<FlatListField> = ArrayList()) {
}

public data class FlatBasicField(
        val qualifiedName: String,
        val type: String,
        val flatBufferType: String)

public data class FlatListField(
        val qualifiedName: String,
        val type: String,
        val parameterType: String,
        val flatBufferType: String
)


fun createFlatClass(e: Element, env: ProcessingEnvironment): FlatClass {

    val typeUtils = env.typeUtils

    Log.trace("Processing Class ${Util.elementToString(e)}")

    if (e is Parameterizable && e.typeParameters.isNotEmpty()) {
        Log.error("Type parameters are not allowed in: ${e.simpleName}")
    }

    val flatType = FlatClass(
            simpleName = e.simpleName.toString(),
            type = Util.typeToString(e.asType()),
            packageName = Util.getPackage(typeUtils.asElement(e.asType())).qualifiedName.toString())

    ElementFilter.fieldsIn(e.enclosedElements).forEach { field ->
        //Process every field in the class
        val type = field.asType()
        val qualifiedFieldName = Util.elementToString(field)
        Log.trace("Processing Field $qualifiedFieldName")

        type.accept(object : SimpleTypeVisitor6<Void, Void>() {
            override fun visitPrimitive(t: PrimitiveType, p: Void?): Void? {
                val typeName = Util.box(t).toString()
                flatType.primitiveFields.add(FlatBasicField(qualifiedFieldName, typeName, t.kind.toFlatType()))
                return null
            }

            override fun visitDeclared(t: DeclaredType, p: Void?): Void? {
                val fieldType = Util.typeToString(t)
                if (fieldType == "java.lang.String") {
                    flatType.primitiveFields.add(FlatBasicField(qualifiedFieldName, fieldType, "string"))
                } else if (fieldType.startsWith("java.util.List") && t.typeArguments.size() == 1) {
                    val typeArg = t.typeArguments.get(0)
                    val parameterType = Util.typeToString(typeArg)

                    if (parameterType == "java.lang.String") {
                        flatType.primitiveListFields.add(FlatListField(
                                qualifiedName = qualifiedFieldName,
                                type = Util.typeToString(t),
                                parameterType = parameterType,
                                flatBufferType = "string"
                        ))
                    } else if (typeArg.kind == TypeKind.DECLARED) {
                        typeArg as DeclaredType
                        if (typeArg.typeArguments.size() > 0) throw IllegalArgumentException("Nested parametrized types are not allowed")
                        flatType.objectListFields.add(FlatListField(
                                qualifiedName = qualifiedFieldName,
                                type = Util.typeToString(t),
                                parameterType = parameterType,
                                flatBufferType = parameterType
                        ))
                    } else {
                        throw IllegalArgumentException("Can't handle $fieldType")
                    }
                } else {
                    flatType.objectFields.add(FlatBasicField(
                            qualifiedName = qualifiedFieldName,
                            type = fieldType,
                            flatBufferType = fieldType))
                }
                return null
            }
        }, null)
    }
    return flatType
}


fun TypeKind.toFlatType(): String {
    return when (this) {
        TypeKind.CHAR -> "short"
        TypeKind.INT -> "int"
        TypeKind.LONG -> "long"
        else -> throw IllegalArgumentException("Unsupported type ${this}")
    }
}

