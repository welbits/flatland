package com.welbits.flatland.processor;

import com.google.flatbuffers.Table;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.CLASS)
public @interface FlatModel {
   Class<? extends Table> value();
}
