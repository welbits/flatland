// automatically generated, do not modify

package com.welbits.flatland.sample.model;

import java.nio.*;
import java.lang.*;
import java.util.*;
import com.google.flatbuffers.*;

public class B_FB extends Table {
  public static B_FB getRootAsB_FB(ByteBuffer _bb) { return getRootAsB_FB(_bb, new B_FB()); }
  public static B_FB getRootAsB_FB(ByteBuffer _bb, B_FB obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__init(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public B_FB __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public A_FB a() { return a(new A_FB()); }
  public A_FB a(A_FB obj) { int o = __offset(4); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }

  public static int createB_FB(FlatBufferBuilder builder,
      int a) {
    builder.startObject(1);
    B_FB.addA(builder, a);
    return B_FB.endB_FB(builder);
  }

  public static void startB_FB(FlatBufferBuilder builder) { builder.startObject(1); }
  public static void addA(FlatBufferBuilder builder, int aOffset) { builder.addOffset(0, aOffset, 0); }
  public static int endB_FB(FlatBufferBuilder builder) {
    int o = builder.endObject();
    return o;
  }
};

