package com.welbits.flatland.sample.model;

import com.welbits.flatland.processor.FlatModel;

import java.util.List;

@FlatModel(A_FB.class)
public class A {
    public int simpleInt;
    public String simpleStringField;
    public B refToB;
    public List<B> listObjects;
    public List<String> listPrimitiveString;
    public List<Integer> listPrimitiveInt;
}