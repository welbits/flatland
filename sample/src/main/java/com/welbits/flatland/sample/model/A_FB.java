// automatically generated, do not modify

package com.welbits.flatland.sample.model;

import java.nio.*;
import java.lang.*;
import java.util.*;
import com.google.flatbuffers.*;

public class A_FB extends Table {
  public static A_FB getRootAsA_FB(ByteBuffer _bb) { return getRootAsA_FB(_bb, new A_FB()); }
  public static A_FB getRootAsA_FB(ByteBuffer _bb, A_FB obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__init(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public A_FB __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public int a() { int o = __offset(4); return o != 0 ? bb.getInt(o + bb_pos) : 0; }

  public static int createA_FB(FlatBufferBuilder builder,
      int a) {
    builder.startObject(1);
    A_FB.addA(builder, a);
    return A_FB.endA_FB(builder);
  }

  public static void startA_FB(FlatBufferBuilder builder) { builder.startObject(1); }
  public static void addA(FlatBufferBuilder builder, int a) { builder.addInt(0, a, 0); }
  public static int endA_FB(FlatBufferBuilder builder) {
    int o = builder.endObject();
    return o;
  }
};

